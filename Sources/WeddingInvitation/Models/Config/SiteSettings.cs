using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WeddingInvitation.Models.Config
{
    public class SiteSettings
    {
        public string SiteUrl { get; set; }
        public int CookieAuthExpireTime { get; set; }

        public string PhotobookPath { get; set; }

        public EmailAccountConfig EmailAccount { get; set; }

    }
}
