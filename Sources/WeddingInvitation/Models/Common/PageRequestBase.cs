using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WeddingInvitation.Models.Common
{
    public class PageRequestBase<SearchT> where SearchT : class
    {
        public SearchT Search { get; set; }

        public SortRequest Sort { get; set; }

        public PaginationRequest Pagination { get; set; }
    }
}
