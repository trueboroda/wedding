using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WeddingInvitation.Models.Common
{
    public class SortRequest
    {

        public string SortBy { get; set; }
        public SortDirection Direction { get; set; }

    }
}
