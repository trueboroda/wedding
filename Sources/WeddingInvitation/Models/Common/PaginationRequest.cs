using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WeddingInvitation.Models.Common
{
    public class PaginationRequest
    {
        public int CurrentPage { get; set; }

        public int PageSize { get; set; }

        public int ItemsToSkip => PageSize * (CurrentPage - 1);
    }
}
