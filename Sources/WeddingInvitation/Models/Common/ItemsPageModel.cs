using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WeddingInvitation.Models.Common
{
    public class ItemsPageModel<T> where T : class
    {
        public int TotalCount { get; set; }


        public IEnumerable<T> Items { get; set; }
    }
}
