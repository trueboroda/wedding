using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WeddingInvitation.Domain;

namespace WeddingInvitation.Models.Auth
{
    public class UserInfoModel
    {
        public int Id { get; set; }
     

        public string Login { get; set; }

        public string Email { get; set; }
      

        public RolesEnum Role { get; set; }
       
    }
}
