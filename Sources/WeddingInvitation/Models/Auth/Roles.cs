using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WeddingInvitation.Models.Auth
{
    public  class Roles
    {
        public const string Admin = "Admin";
        public const string Observer = "Observer";
    }
}
