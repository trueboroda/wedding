using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WeddingInvitation.Models.Auth
{
    public class AuthenticationResult
    {
        /// <summary>
        /// Найденный юзер
        /// </summary>
        public Domain.User User { get; set; }

        /// <summary>
        /// Успех операции
        /// </summary>
        public bool Success { get; set; }

        /// <summary>
        /// Сообщение об ошибке
        /// </summary>
        public string ErrorMessage { get; set; }
    }
}
