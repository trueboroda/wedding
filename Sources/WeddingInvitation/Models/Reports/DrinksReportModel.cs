using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WeddingInvitation.Models.Guest;

namespace WeddingInvitation.Models.Reports
{
    public class DrinksReportModel
    {
        public string Drink { get; set; }

        public int Count { get; set; }

        public GuestModel[] Guests { get; set; }

    }
}
