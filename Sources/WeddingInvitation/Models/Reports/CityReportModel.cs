using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WeddingInvitation.Models.Guest;

namespace WeddingInvitation.Models.Reports
{
    public class CityReportModel
    {
        public string City { get; set; }
        public int AnketCount { get; set; }

        public int GuestsCount { get; set; }

        public int ChildrenCount { get; set; }

        public GuestModel[] Guests { get; set; }
    }
}
