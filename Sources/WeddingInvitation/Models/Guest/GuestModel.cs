using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WeddingInvitation.Domain;

namespace WeddingInvitation.Models.Guest
{
    public class GuestModel
    {
        public int Id { get; set; }

        public string Code { get; set; }
        
        public string Title { get; set; }

        public WhoseGuestEnum Whose { get; set; }

        public bool IsNonresident { get; set; }

        public string Description { get; set; }
    }
}
