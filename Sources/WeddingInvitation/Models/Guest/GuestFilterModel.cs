using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WeddingInvitation.Domain;

namespace WeddingInvitation.Models.Guest
{
    public class GuestFilterModel
    {
        public string Text { get; set; }

        public bool? IsConfirmed { get; set; }

        public WhoseGuestEnum? Whose { get; set; }

        public bool? IsNonresident { get; set; }

        public bool? IsChanged { get; set; }

        public bool? WithPair { get; set; }

        public bool? WithChildren { get; set; }

        public bool? BehindTheWheel { get; set; }


    }
}
