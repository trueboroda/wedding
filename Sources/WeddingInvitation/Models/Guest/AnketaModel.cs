using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WeddingInvitation.Models.Guest
{
    public class AnketaModel
    {
        public int GuestId { get; set; }

        public Boolean IsConfirmed { get; set; }

        public string Phones { get; set; }

        public string Comment { get; set; }

        public bool? WithPair { get; set; }

        public bool WithChildren { get; set; }

        public bool BehindTheWheel { get; set; }

        public int? DrinkId1 { get; set; }
                
        public int? VineTypeId1 { get; set; }
                

        public int? DrinkId2 { get; set; }
                

        public int? VineTypeId2 { get; set; }
        
        
        public int? CityId { get; set; }
        

        public DateTime? ArrivalDate { get; set; }

        public DateTime? DepartureDate { get; set; }
    }
}
