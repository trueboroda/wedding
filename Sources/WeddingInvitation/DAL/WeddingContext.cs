using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WeddingInvitation.Domain;

namespace WeddingInvitation.DAL
{
    public class WeddingContext
        :DbContext
    {

        public DbSet<User> Users { get; set; }

        public DbSet<Guest> Guests { get; set; }

        public DbSet<Drink> Drinks { get; set; }

        public DbSet<VineType> VineTypes { get; set; }

        public DbSet<City> Cities { get; set; }


        public DbSet<GuestAnketa> GuestAnketas { get; set; }


        public WeddingContext(DbContextOptions<WeddingContext> options)
            :base(options)
        {
            Database.Migrate();
        }


        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<Guest>()
                .HasIndex(x => x.Code).IsUnique();

            modelBuilder.Entity<Guest>()
                .HasOne(x => x.Anketa).WithOne(a => a.Guest)
                .HasForeignKey<GuestAnketa>(a => a.GuestId);


            modelBuilder.Entity<GuestAnketa>()
                .Property(x => x.ChangingTime).HasDefaultValue(new DateTime(2018, 08, 07, 0, 0, 0, DateTimeKind.Utc));

            modelBuilder.Entity<GuestAnketa>()
                .Property(x => x.CreationTime).HasDefaultValue(new DateTime(2018, 08, 07, 0, 0, 0, DateTimeKind.Utc));
          
        }
    }
}
