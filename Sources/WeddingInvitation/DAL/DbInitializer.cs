using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WeddingInvitation.Abstractions;
using WeddingInvitation.Domain;

namespace WeddingInvitation.DAL
{
    public class DbInitializer
    {
        private readonly WeddingContext _context;
        private readonly IHashService _hashService;        

        public DbInitializer(WeddingContext context, IHashService hashService)
        {
            _context = context;
            _hashService = hashService;
        }

        public  void Seed()
        {
            if (!_context.Users.Any())
            {
                var admin = new User()
                {
                    Login = "trueboroda",
                    Email = "superiorboroda@gmail.com",
                    PasswordHash = _hashService.HashPassword("scarface"),
                    Role = RolesEnum.Admin
                };
                _context.Users.Add(admin);
                var observer = new User()
                {
                    Login = "anna",
                    Email = "anna.zhitluhina@yandex.ru",
                    PasswordHash = _hashService.HashPassword("scarface"),
                    Role = RolesEnum.Admin
                };

                _context.Users.AddRange(admin, observer);
                _context.SaveChanges();
            }


            if (!_context.Drinks.Any())
            {
                _context.Drinks.AddRange(
                      new Drink() { Id = 1, Name = "Безалкоголные" }
                    , new Drink() { Id = 2, Name = "Вино" }                    
                    , new Drink() { Id = 3, Name = "Водка" }
                    , new Drink() { Id = 4, Name = "Коньяк" }
                    , new Drink() { Id = 5, Name = "Виски или скотч" }
                    );
                _context.SaveChanges();
            }

            if (!_context.VineTypes.Any())
            {
                _context.VineTypes.AddRange(
                  new VineType() { Id = 1, Name = "Белое полусладкое" }
                , new VineType() { Id = 2, Name = "Белое сухое" }
                , new VineType() { Id = 3, Name = "Красное полусладкое" }
                , new VineType() { Id = 4, Name = "Красное сухое" });
                _context.SaveChanges();
            }

            if (!_context.Cities.Any())
            {
                _context.Cities.AddRange(new City() { Id = 1, Name = "Киров" }
                , new City() { Id = 2, Name = "Кирово-Чепецк" });
                _context.SaveChanges();
            }

        }

    }
}
