using AutoMapper;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.Extensions.Options;
using WeddingInvitation.Abstractions;
using WeddingInvitation.DAL;
using WeddingInvitation.Domain;
using WeddingInvitation.Models.Auth;
using WeddingInvitation.Models.Config;

namespace WeddingInvitation.Controllers
{
    [Route("api")]    
    public class ServiceController : ControllerBase
    {
        private readonly WeddingContext _context;
        private readonly IAuthenticator _auth;
        private readonly IMapper _mapper;

        private readonly SiteSettings _settings;

        public ServiceController(WeddingContext context, IAuthenticator auth, IMapper mapper, IOptions<SiteSettings> settingsOptions)
        {
            _context = context;
            _auth = auth;
            _mapper = mapper;
            _settings = settingsOptions.Value;
        }


        // GET api/values/5
        [HttpGet()]
        [Route("site_url")]
        public ActionResult<string> GetSiteUrl()
        {
            return _settings.SiteUrl;
        }



        // GET api/values/5
        [HttpGet()]
        [Route("ping")]
        public ActionResult<string> Ping()
        {
            return "ping Ok";
        }

        [HttpPost]
        [Route("login")]
        public async Task<IActionResult> Login([FromBody]LoginModel model)
        {
            var name = model.Login;
            var password = model.Password;

            var authenticationResult = _auth.Authenticate(name, password);
            if (authenticationResult.Success)
            {
                User user = authenticationResult.User;

                await SignIn(user);
                var userInfo = _mapper.Map<UserInfoModel>(user);
                return Ok(userInfo);
            }
            else
            {
                return NotFound(authenticationResult.ErrorMessage);
            }
        }

        private async Task SignIn(User user)
        {
            var claims = new List<Claim>()
                    {
                        new Claim(ClaimTypes.NameIdentifier, user.Id.ToString()),
                        new Claim(ClaimTypes.Name, string.IsNullOrEmpty(user.Login)? user.Email : user.Login)
                    };

            claims.Add(new Claim(ClaimTypes.Role, RolesEnum.Admin.ToString()));

            var identity = new ClaimsIdentity(claims, "ApplicationCookie");
            await HttpContext.SignInAsync(CookieAuthenticationDefaults.AuthenticationScheme, new ClaimsPrincipal(identity));
        }

        private async void SignOut()
        {
            await HttpContext.SignOutAsync(CookieAuthenticationDefaults.AuthenticationScheme);
        }

        [Authorize]
        [HttpGet]
        [Route("logout")]
        public IActionResult Logout()
        {
            SignOut();
            return Ok();
        }

        [Authorize]
        [HttpGet]
        [Route("userinfo")]
        public IActionResult GetUserInfo()
        {
            var userClaim = HttpContext.User.FindFirst(ClaimTypes.NameIdentifier);
            if (userClaim != null)
            {
                var userId = int.Parse(userClaim.Value);
                var user = _context.Users.FirstOrDefault(x=>x.Id == userId);

                var userInfo = _mapper.Map<UserInfoModel>(user);
                

                return Ok(userInfo);
            }
            return NotFound();
        }
    }
}
