using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using WeddingInvitation.Models.Config;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace WeddingInvitation.Controllers
{
    [Route("api/[controller]")]
    public class PhotosController : Controller
    {
         string _photobookPath;
         string _relativePath;

        private  const string WWWROOT = "wwwroot";
        public PhotosController(IOptions<SiteSettings> settingsOptions, IHostingEnvironment hostingEnvironment)
        {
            var settings = settingsOptions.Value;

            _relativePath = settings.PhotobookPath;
            var contentRootPath = hostingEnvironment.ContentRootPath;

            _photobookPath = Path.Combine(contentRootPath, WWWROOT, _relativePath);

        }

        // GET: api/<controller>
        [HttpGet]
        public IEnumerable<string> Get()
        {

            var photosServerPaths = Directory.EnumerateFiles(_photobookPath);
            var webPaths = photosServerPaths.Select(x => {
                var fileName =  Path.GetFileName(x);
                var relativePhotoPath = Path.Combine(_relativePath, fileName);
                return relativePhotoPath.Replace(Path.DirectorySeparatorChar, '/');
            }).ToArray();
            
            return webPaths;
        }        
      
    }
}
