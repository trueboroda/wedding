using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System.Collections.Generic;
using System.Linq;
using WeddingInvitation.DAL;
using WeddingInvitation.Models.Guest;
using WeddingInvitation.Models.Reports;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace WeddingInvitation.Controllers
{
    [Route("api/[controller]")]
    public class ReportsController : Controller
    {
        private readonly WeddingContext _context;
        private readonly IMapper _mapper;
        private readonly ILogger _logger;

        public ReportsController(WeddingContext context, IMapper mapper, ILogger<GuestsController> logger)
        {
            _context = context;
            _mapper = mapper;
            _logger = logger;
        }


        [HttpGet]
        [Route("city")]
        public IEnumerable<CityReportModel> GetCityReport()
        {
            var report = new List<CityReportModel>();

            var cities = _context.Cities.ToArray();

            foreach (var city in cities)
            {
                var anketCount = _context.Guests.Count(x => x.Anketa.IsConfirmed == true && x.Anketa.CityId == city.Id);
                var withPairCount = _context.Guests.Count(x => x.Anketa.IsConfirmed == true && x.Anketa.WithPair == true && x.Anketa.CityId == city.Id);
                var aloneCount = _context.Guests.Count(x => x.Anketa.IsConfirmed == true && x.Anketa.WithPair == false&& x.Anketa.CityId == city.Id);
                var childredCount = _context.Guests.Count(x => x.Anketa.IsConfirmed == true && x.Anketa.WithChildren == true && x.Anketa.CityId == city.Id);
                var guests = _context.Guests.Where(x => x.Anketa.IsConfirmed == true && x.Anketa.CityId == city.Id)
                    .Select(x => _mapper.Map<GuestModel>(x)).ToArray();


                report.Add(new CityReportModel()
                {
                    City = city.Name,
                    AnketCount = anketCount,
                    GuestsCount = withPairCount * 2 + aloneCount,
                    ChildrenCount = childredCount,
                    Guests = guests
                });
            }

            return report;
        }



        // GET: api/<controller>
        [HttpGet]
        [Route("answers")]
        public IEnumerable<AnswersReportModel> GetAnswersReport()
        {
            var report = new List<AnswersReportModel>();


            var yesCount = _context.Guests.Count(x => x.Anketa.IsConfirmed == true);
            var withPairCount = _context.Guests.Count(x => x.Anketa.IsConfirmed == true && x.Anketa.WithPair == true);
            var aloneCount = _context.Guests.Count(x => x.Anketa.IsConfirmed == true && x.Anketa.WithPair == false);
            var childredCount = _context.Guests.Count(x => x.Anketa.IsConfirmed == true && x.Anketa.WithChildren == true);

            var yestGuests = _context.Guests.Where(x => x.Anketa.IsConfirmed == true)
                .Select(x => _mapper.Map<GuestModel>(x)).ToArray();

            report.Add(new AnswersReportModel()
            {
                Answer = "Да",
                AnketCount = yesCount,
                GuestsCount = withPairCount*2 + aloneCount,
                ChildrenCount = childredCount,
                Guests = yestGuests
            });


            var noCount = _context.Guests.Count(x => x.Anketa.IsConfirmed == false);
            var noGuests = _context.Guests.Where(x => x.Anketa.IsConfirmed == false)
                .Select(x => _mapper.Map<GuestModel>(x)).ToArray();

            report.Add(new AnswersReportModel()
            {
                Answer = "Нет",
                AnketCount = noCount,
                Guests = noGuests
            });

            var undefCount = _context.Guests.Count(x => x.Anketa == null);
            var undefGuests = _context.Guests.Where(x => x.Anketa == null)
                .Select(x => _mapper.Map<GuestModel>(x)).ToArray();
            report.Add(new AnswersReportModel()
            {
                Answer = "Не ответили",
                AnketCount = undefCount,
                Guests = undefGuests
            });

            return report;
        }

        [HttpGet]
        [Route("drinks")]
        public IEnumerable<DrinksReportModel> GetDrinksReport()
        {

            /*
             { Id = 1, Name = "Безалкоголные" }
             { Id = 2, Name = "Вино" }           
             { Id = 3, Name = "Водка" }
             { Id = 4, Name = "Коньяк" }
             { Id = 5, Name = "Виски или скотч" }

             { Id = 1, Name = "Белое полусладкое" }
             { Id = 2, Name = "Белое сухое" }
             { Id = 3, Name = "Красное полусладкое" }
             { Id = 4, Name = "Красное сухое" });
             */

            var report = new List<DrinksReportModel>();

            var noAlcoRecord = GetDrinkReportRecord("Безалкоголные",1);
            var whiteSemiSweetVineRecord = GetDrinkReportRecord("Белое полусладкое", 2, 1);
            var whiteDryVineRecord = GetDrinkReportRecord("Белое сухое", 2, 2);
            var redSemiSweetVineRecord = GetDrinkReportRecord("Красное полусладкое", 2, 3);
            var redDryVineRecord = GetDrinkReportRecord("Красное сухое", 2, 4);
            var vodkaRecord = GetDrinkReportRecord("Водка", 3);
            var cognacRecord = GetDrinkReportRecord("Коньяк", 4);
            var wiskyRecord = GetDrinkReportRecord("Виски или скотч", 5);

            report.AddRange(new []{ noAlcoRecord
                ,whiteSemiSweetVineRecord
                ,whiteDryVineRecord
                ,redSemiSweetVineRecord
                ,redDryVineRecord
                ,vodkaRecord
                ,cognacRecord
                ,wiskyRecord
            });
             
            return report;
        }


        private DrinksReportModel GetDrinkReportRecord(string name, int drinkId, int? vineType = null)
        {
            var count1 = _context.Guests
                .Count(x => x.Anketa.IsConfirmed && x.Anketa.DrinkId1 == drinkId && (vineType == null || (x.Anketa.VineTypeId1 == vineType)));
            var count2 = _context.Guests
                .Count(x => x.Anketa.IsConfirmed && x.Anketa.DrinkId2 == drinkId && (vineType == null || (x.Anketa.VineTypeId2 == vineType)));
            var guests =  _context.Guests
                .Where(x => x.Anketa.IsConfirmed &&
                          ( (x.Anketa.DrinkId1 == drinkId && (vineType == null || (x.Anketa.VineTypeId1 == vineType)) )
                           || ( x.Anketa.DrinkId2 == drinkId && (vineType == null || (x.Anketa.VineTypeId2 == vineType)) )
                          )
                      )
                 .Select(x=> _mapper.Map<GuestModel>(x)).ToArray();

            return new DrinksReportModel()
            {
                Drink = name,
                Count = count1 + count2,
                Guests = guests
            };

        }
    }
}
