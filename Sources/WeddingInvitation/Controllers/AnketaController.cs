using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using WeddingInvitation.DAL;
using WeddingInvitation.Domain;
using WeddingInvitation.Models.Guest;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace WeddingInvitation.Controllers
{
    [Route("api/[controller]")]
    public class AnketaController : Controller
    {

        private readonly WeddingContext _context;
        private readonly IMapper _mapper;
        private readonly ILogger _logger;

        public AnketaController(WeddingContext context, IMapper mapper, ILogger<GuestsController> logger)
        {
            _context = context;
            _mapper = mapper;
            _logger = logger;
        }

        [HttpGet]
        [Route("drinks")]
        public IEnumerable<Drink> GetDrinks()
        {
            var drinks = _context.Drinks.ToArray();

            return drinks;
        }


        [HttpGet]
        [Route("vinetypes")]
        public IEnumerable<VineType> GetVineTypes()
        {
            var vineTypes = _context.VineTypes.ToArray();

            return vineTypes;
        }


        [HttpGet]
        [Route("cities")]
        public IEnumerable<City> GetCities()
        {
            var cities = _context.Cities.ToArray();

            return cities;
        }

        [HttpGet("{guestid}")]
        public AnketaModel GetByGuest(int guestId)
        {
            var anketa = _context.GuestAnketas.FirstOrDefault(x=>x.GuestId == guestId);

            if(anketa == null)
            {
                return null;
            }

            var model = _mapper.Map<AnketaModel>(anketa);

            return model;
        }


        [HttpPost]
        public IActionResult Merge([FromBody]AnketaModel model)
        {

            if(model == null)
            {
                return BadRequest("Пустая анкета");
            }

            var anketa = _context.GuestAnketas.FirstOrDefault(x=>x.GuestId == model.GuestId);

            if(anketa == null)
            {
                anketa = _mapper.Map<GuestAnketa>(model);
                anketa.CreationTime = DateTime.UtcNow;
                anketa.ChangingTime = DateTime.UtcNow;
                anketa.IsChanged = true;
                _context.Add(anketa);
            }
            else
            {
                anketa.ChangingTime = DateTime.UtcNow;
                anketa.IsChanged = true;
                _mapper.Map(model, anketa);
                _context.Entry(anketa);
            }

            _context.SaveChanges();

            return Ok();
        }


        [HttpPut]
        [Route("as_viewed/{guestId}")]
        public IActionResult MarkAsViewed(int guestId)
        {
            var anketa = _context.GuestAnketas.FirstOrDefault(x=>x.GuestId == guestId);

            if (anketa == null)
            {
                return NotFound();
            }

            anketa.IsChanged = false;

            _context.SaveChanges();


            return Ok();
        }
    }
}
