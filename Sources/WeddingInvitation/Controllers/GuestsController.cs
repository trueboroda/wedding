using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using System.Linq;
using WeddingInvitation.DAL;
using WeddingInvitation.Domain;
using WeddingInvitation.Models.Common;

using System.Linq.Dynamic.Core;
using Microsoft.EntityFrameworkCore;
using WeddingInvitation.Models.Guest;
using System.Collections.Generic;
using Microsoft.Extensions.Logging;
using System;
using Microsoft.AspNetCore.Authorization;
using WeddingInvitation.Models.Auth;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace WeddingInvitation.Controllers
{
    [Authorize(Roles = Roles.Admin)]
    [Route("api/[controller]")]
    public class GuestsController : Controller
    {
        private readonly WeddingContext _context;
        private readonly IMapper _mapper;
        private readonly ILogger _logger;

        public GuestsController(WeddingContext context, IMapper mapper, ILogger<GuestsController> logger)
        {
            _context = context;
            _mapper = mapper;
            _logger = logger;
        }


        [HttpGet]
        [AllowAnonymous]
        [Route("{code}")]
        public IActionResult GetByCode(string code)
        {
            if(!Guid.TryParse(code, out Guid parsed))
            {
                return BadRequest("Не удалось распарсить код");

            }

            var guest = _context.Guests.FirstOrDefault(x => x.Code == parsed);
            if(guest == null)
            {
                return NotFound();
            }

            var model = _mapper.Map<GuestModel>(guest);
            return Ok(model);

        }

        //review/get_page
        [HttpPost]
        [Route("review/get_page")]
        public ItemsPageModel<Guest> GetReviewPage([FromBody]PageRequestBase<GuestFilterModel> request)
        {
            var q = _context.Guests.Include(x=>x.Anketa).AsQueryable();

            var filter = request.Search;

            if (filter != null)
            {
                if (!string.IsNullOrEmpty(request.Search?.Text))
                {
                    q = q.Where(x => x.Title.Contains(filter.Text)
                        || x.Description.Contains(filter.Text)
                        || x.Code.ToString().Contains(filter.Text)
                        );
                }
                if (filter.IsNonresident.HasValue)
                {
                    q = q.Where(x => x.IsNonresident == filter.IsNonresident);
                }
                if (filter.IsConfirmed.HasValue)
                {
                    q = q.Where(x => x.Anketa.IsConfirmed == filter.IsConfirmed);
                }
                if (filter.IsChanged.HasValue)
                {
                    q = q.Where(x => x.Anketa.IsChanged == filter.IsChanged);
                }
               
                if (filter.WithPair.HasValue)
                {
                    q = q.Where(x => x.Anketa.WithPair == filter.WithPair);
                }
                if (filter.WithChildren.HasValue)
                {
                    q = q.Where(x => x.Anketa.WithChildren == filter.WithChildren);
                }
                if (filter.BehindTheWheel.HasValue)
                {
                    q = q.Where(x => x.Anketa.BehindTheWheel == filter.BehindTheWheel);
                }
                if(filter.Whose.HasValue)
                {
                    q = q.Where(x => x.Whose == filter.Whose);
                }


            }



            if (!string.IsNullOrEmpty(request.Sort?.SortBy))
            {
                string strDirection = request.Sort.Direction == (int)SortDirection.Ascending ? "ASC" : "DESC";
                string sortStr = request.Sort.SortBy + " " + strDirection;
                q = q.OrderBy(sortStr);
            }
            else
            {
                q = q.OrderByDescending(p => p.Id);
            }


            var totalCount = q.Count();

            var list =  q.Skip(request.Pagination.ItemsToSkip).Take(request.Pagination.PageSize).ToList();


            //List<GuestModel> models = _mapper.Map<IEnumerable<Guest>, List<GuestModel>>(list);

            return new ItemsPageModel<Guest>()
            {
                Items = list,
                TotalCount = totalCount
            };

        }


        [HttpPost]
        [Route("get_page")]
        public ItemsPageModel<GuestModel> GetPage([FromBody]TextFilterPageRequest request)
        {
            var q = _context.Guests.AsQueryable();


            if(!string.IsNullOrEmpty(request.Search?.Text))
            {
                q = q.Where(x => x.Title.Contains(request.Search.Text)
                    || x.Description.Contains(request.Search.Text)
                    || x.Code.ToString().Contains(request.Search.Text)
                    );
            }

            if (!string.IsNullOrEmpty( request.Sort?.SortBy))
            {
                string strDirection = request.Sort.Direction == (int)SortDirection.Ascending ? "ASC" : "DESC";
                string sortStr = request.Sort.SortBy + " " + strDirection;
                q = q.OrderBy(sortStr);
            }
            else
            {
                q = q.OrderByDescending(p => p.Id);
            }


            var totalCount = q.Count();

            var list =  q.Skip(request.Pagination.ItemsToSkip).Take(request.Pagination.PageSize).ToList();


            List<GuestModel> models = _mapper.Map<IEnumerable<Guest>, List<GuestModel>>(list);

            return new ItemsPageModel<GuestModel>()
            {
                Items = models,
                TotalCount = totalCount
            };

        }




        /// <summary>
        /// Добавление
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        [HttpPost]
        public IActionResult Post([FromBody]GuestModel value)
        {
            if (value.Id > 0)
            {
                return BadRequest("Id must be zero!");
            }

            var g =  _mapper.Map<GuestModel, Guest>(value);

            g.Code = Guid.NewGuid();
            _context.Guests.Add(g);
            _context.SaveChanges();
            _logger.LogInformation($"Приглашение {g.Code} успешно добавлено.");
            var returnModel = _mapper.Map<GuestModel>(g);
            return Ok(returnModel);
        }

        /// <summary>
        /// Обновление
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        [HttpPut()]
        public IActionResult Put([FromBody]GuestModel value)
        {
            
                if (value.Id == 0)
                {
                    return BadRequest("Id is not setted");
                }

                var g = _context.Guests.FirstOrDefault(x=>x.Id == value.Id);
                if (g == null)
                {
                    return NotFound("Guest is not found");
                }

                _mapper.Map(value, g);

            _context.SaveChanges();
            _logger.LogInformation($"Приглашение {g.Code} успешно изменено.");

            return Ok();
            
        }

        // DELETE api/values/5
        [HttpDelete()]
        public IActionResult Delete(int id)
        {

            if (id == 0)
            {
                return BadRequest("Id is not setted");
            }

            var g = _context.Guests.FirstOrDefault(x=>x.Id == id);
            if(g == null)
            {
                    return NotFound("Guest is not found");
            }

            _context.Remove(g);
            _context.SaveChanges();

            _logger.LogInformation($"Приглашение {g.Code} успешно удалено.");

            return Ok();
            
        }


    }
}
