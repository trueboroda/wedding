using Microsoft.Extensions.Logging;
using System;
using System.Linq;
using System.Threading.Tasks;
using WeddingInvitation.Abstractions;
using WeddingInvitation.DAL;
using WeddingInvitation.Models.Auth;

namespace WeddingInvitation.Services
{
    public class Authenticator
        : IAuthenticator
    {
        private readonly WeddingContext _context;
        private readonly ILogger _logger;
        private readonly IHashService _hashService;

        public Authenticator(WeddingContext context, ILogger<Authenticator> logger, IHashService hashService)
        {
            _context = context;
            _hashService = hashService;
            _logger = logger;
        }

        public AuthenticationResult Authenticate(string username, string password)
        {

            var result = new AuthenticationResult()
            {
                Success = false
            };

            if (string.IsNullOrWhiteSpace(username) || string.IsNullOrWhiteSpace(password))
            {
                result.ErrorMessage = "Укажите имя пользователя и пароль";
                return result;
            }

            username = username.Trim();
            var user = _context.Users.FirstOrDefault(u => u.Login == username || u.Email == username);

            if(user == null)
            {
                result.ErrorMessage = "Пользователь с таким именем не существует";
                return result;
            }
            else
            {
                if (_hashService.VerifyPassword(user.PasswordHash, password))
                {
                    _logger.LogInformation($"Пользователь {username} успешно прошёл проверку подлинности.");                                        
                    result.Success = true;
                    result.User = user;
                    return result;
                }
                result.ErrorMessage = "Не верный пароль!";
                return result;
            }

        }
    }
}
