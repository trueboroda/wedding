using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using WeddingInvitation.Abstractions;

namespace WeddingInvitation.Services
{
    public class HashService
        :IHashService
    {


        public const int SAULT_LENGTH = 8;

        /// <summary>
        /// Генератор соли
        /// </summary>
        /// <returns></returns>
        private static string GenerateSaltValue()
        {
            var encoding = new UnicodeEncoding();

            if (encoding != null)
            {
                // Create a random number object seeded from the value
                // of the last random seed value. This is done
                // interlocked because it is a static value and we want
                // it to roll forward safely.
                Random random = new Random(unchecked((int)DateTime.Now.Ticks));

                if (random != null)
                {
                    // Create an array of random values.
                    byte[] saltValue = new byte[SAULT_LENGTH * UnicodeEncoding.CharSize];

                    random.NextBytes(saltValue);

                    // Convert the salt value to a string. Note that the resulting string
                    // will still be an array of binary values and not a printable string. 
                    // Also it does not convert each byte to a double byte.
                    string saltValueString = encoding.GetString(saltValue);

                    // Return the salt value as a string.
                    return saltValueString;
                }
            }

            return null;
        }

        public string HashPassword(string password)
        {
            var sault = GenerateSaltValue();
            return HashPassword(password, sault);
        }

        public string HashPassword(string password, string saltValue)
        {
            // If the salt string is null or the length is invalid then
            // create a new valid salt value.
            if (string.IsNullOrEmpty(saltValue))
            {
                throw new ArgumentException();
            }

            var encoding = new UnicodeEncoding();
            HashAlgorithm hash = HashAlgorithm.Create("SHA256");

            if (password != null && hash != null && encoding != null)
            {
                // Convert the salt string and the password string to a single
                // array of bytes. Note that the password string is Unicode and
                // therefore may or may not have a zero in every other byte.
                byte[] binarySaltValue = encoding.GetBytes(saltValue);
                byte[] binaryPassword = encoding.GetBytes(password);

                byte[] valueToHash = new byte[SAULT_LENGTH * UnicodeEncoding.CharSize + binaryPassword.Length];

                // Copy the salt value and the password to the hash buffer.
                binarySaltValue.CopyTo(valueToHash, 0);
                binaryPassword.CopyTo(valueToHash, SAULT_LENGTH * UnicodeEncoding.CharSize);

                byte[] hashValue = hash.ComputeHash(valueToHash);

                // The hashed password is the salt plus the hash value (as a string).
                string hashedPassword = saltValue;

                foreach (byte hexdigit in hashValue)
                {
                    hashedPassword += hexdigit.ToString("X2", CultureInfo.InvariantCulture.NumberFormat);
                }

                // Return the hashed password as a string.
                return hashedPassword;
            }

            return null;
        }

        public bool VerifyPassword(string validHashedPassword, string password)
        {
            if (string.IsNullOrEmpty(validHashedPassword) ||
                string.IsNullOrEmpty(password) ||
                validHashedPassword.Length < SAULT_LENGTH)
            {
                return false;
            }

            // Strip the salt value off the front of the stored password.
            string saltValue = validHashedPassword.Substring(0, SAULT_LENGTH);

            var hashedPassword = HashPassword(password, saltValue);

            return validHashedPassword.Equals(hashedPassword, StringComparison.Ordinal);
        }

    }
}
