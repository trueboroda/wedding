using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using WeddingInvitation.Domain;
using WeddingInvitation.Models.Auth;
using WeddingInvitation.Models.Guest;

namespace WeddingInvitation
{
    public class MapperConfigurator
    {
        public static void Configure(IMapperConfigurationExpression cfg)
        {
            cfg.CreateMap<User, UserInfoModel>();

            cfg.CreateMap<Guest, GuestModel>().ForMember(x => x.Code, o => o.MapFrom(s=>s.Code.ToString()));
            cfg.CreateMap<GuestModel, Guest>().ForMember(x => x.Code, o => o.Ignore());


            cfg.CreateMap<GuestAnketa, AnketaModel>();
            cfg.CreateMap<AnketaModel, GuestAnketa>();
        }
    }
}
