﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.Extensions.DependencyInjection;

namespace WeddingInvitation
{
    public static class ServiceProviderExtensions
    {

        public static void AddAutoMapper(this IServiceCollection services)
        {
            var config = new MapperConfiguration(MapperConfigurator.Configure);
            services.AddSingleton<IMapper>(new Mapper(config));
        }
    }
}
