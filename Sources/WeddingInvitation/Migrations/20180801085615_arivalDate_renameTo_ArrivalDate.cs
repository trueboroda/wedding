﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace WeddingInvitation.Migrations
{
    public partial class arivalDate_renameTo_ArrivalDate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "ArivalDate",
                table: "GuestAnketas",
                newName: "ArrivalDate");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "ArrivalDate",
                table: "GuestAnketas",
                newName: "ArivalDate");
        }
    }
}
