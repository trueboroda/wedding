﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace WeddingInvitation.Migrations
{
    public partial class ChangingFields_for_GuestAnketa_adding : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<DateTime>(
                name: "ChangingTime",
                table: "GuestAnketas",
                nullable: false,
                defaultValue: new DateTime(2018, 8, 7, 0, 0, 0, 0, DateTimeKind.Utc));

            migrationBuilder.AddColumn<DateTime>(
                name: "CreationTime",
                table: "GuestAnketas",
                nullable: false,
                defaultValue: new DateTime(2018, 8, 7, 0, 0, 0, 0, DateTimeKind.Utc));

            migrationBuilder.AddColumn<bool>(
                name: "IsChanged",
                table: "GuestAnketas",
                nullable: false,
                defaultValue: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ChangingTime",
                table: "GuestAnketas");

            migrationBuilder.DropColumn(
                name: "CreationTime",
                table: "GuestAnketas");

            migrationBuilder.DropColumn(
                name: "IsChanged",
                table: "GuestAnketas");
        }
    }
}
