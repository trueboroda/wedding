﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace WeddingInvitation.Migrations
{
    public partial class BehindTheWheel_adding_to_anketa : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<bool>(
                name: "IsChanged",
                table: "GuestAnketas",
                nullable: false,
                oldClrType: typeof(bool),
                oldDefaultValue: true);

            migrationBuilder.AddColumn<bool>(
                name: "BehindTheWheel",
                table: "GuestAnketas",
                nullable: false,
                defaultValue: false);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "BehindTheWheel",
                table: "GuestAnketas");

            migrationBuilder.AlterColumn<bool>(
                name: "IsChanged",
                table: "GuestAnketas",
                nullable: false,
                defaultValue: true,
                oldClrType: typeof(bool));
        }
    }
}
