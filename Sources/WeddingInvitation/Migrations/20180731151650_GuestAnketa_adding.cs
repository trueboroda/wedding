using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace WeddingInvitation.Migrations
{
    public partial class GuestAnketa_adding : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "IsNonresident",
                table: "Guests",
                nullable: false,
                defaultValue: false);

            migrationBuilder.CreateTable(
                name: "GuestAnketas",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    GuestId = table.Column<int>(nullable: false),
                    IsConfirmed = table.Column<bool>(nullable: false),
                    Phones = table.Column<string>(nullable: true),
                    Comment = table.Column<string>(nullable: true),
                    WithPair = table.Column<bool>(nullable: false),
                    WithChildren = table.Column<bool>(nullable: false),
                    DrinkId1 = table.Column<int>(nullable: false),
                    VineTypeId1 = table.Column<int>(nullable: true),
                    DrinkId2 = table.Column<int>(nullable: true),
                    VineTypeId2 = table.Column<int>(nullable: true),
                    CityId = table.Column<int>(nullable: false),
                    ArivalDate = table.Column<DateTime>(nullable: true),
                    DepartureDate = table.Column<DateTime>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_GuestAnketas", x => x.Id);
                    table.ForeignKey(
                        name: "FK_GuestAnketas_Cities_CityId",
                        column: x => x.CityId,
                        principalTable: "Cities",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_GuestAnketas_Drinks_DrinkId1",
                        column: x => x.DrinkId1,
                        principalTable: "Drinks",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_GuestAnketas_Drinks_DrinkId2",
                        column: x => x.DrinkId2,
                        principalTable: "Drinks",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_GuestAnketas_Guests_GuestId",
                        column: x => x.GuestId,
                        principalTable: "Guests",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_GuestAnketas_VineTypes_VineTypeId1",
                        column: x => x.VineTypeId1,
                        principalTable: "VineTypes",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_GuestAnketas_VineTypes_VineTypeId2",
                        column: x => x.VineTypeId2,
                        principalTable: "VineTypes",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_GuestAnketas_CityId",
                table: "GuestAnketas",
                column: "CityId");

            migrationBuilder.CreateIndex(
                name: "IX_GuestAnketas_DrinkId1",
                table: "GuestAnketas",
                column: "DrinkId1");

            migrationBuilder.CreateIndex(
                name: "IX_GuestAnketas_DrinkId2",
                table: "GuestAnketas",
                column: "DrinkId2");

            migrationBuilder.CreateIndex(
                name: "IX_GuestAnketas_GuestId",
                table: "GuestAnketas",
                column: "GuestId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_GuestAnketas_VineTypeId1",
                table: "GuestAnketas",
                column: "VineTypeId1");

            migrationBuilder.CreateIndex(
                name: "IX_GuestAnketas_VineTypeId2",
                table: "GuestAnketas",
                column: "VineTypeId2");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "GuestAnketas");

            migrationBuilder.DropColumn(
                name: "IsNonresident",
                table: "Guests");
        }
    }
}
