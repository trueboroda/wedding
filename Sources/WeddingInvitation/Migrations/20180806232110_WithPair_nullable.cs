﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace WeddingInvitation.Migrations
{
    public partial class WithPair_nullable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<bool>(
                name: "WithPair",
                table: "GuestAnketas",
                nullable: true,
                oldClrType: typeof(bool));
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<bool>(
                name: "WithPair",
                table: "GuestAnketas",
                nullable: false,
                oldClrType: typeof(bool),
                oldNullable: true);
        }
    }
}
