﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace WeddingInvitation.Migrations
{
    public partial class drinkId1_n_cityId_nullable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_GuestAnketas_Cities_CityId",
                table: "GuestAnketas");

            migrationBuilder.DropForeignKey(
                name: "FK_GuestAnketas_Drinks_DrinkId1",
                table: "GuestAnketas");

            migrationBuilder.AlterColumn<int>(
                name: "DrinkId1",
                table: "GuestAnketas",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AlterColumn<int>(
                name: "CityId",
                table: "GuestAnketas",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AddForeignKey(
                name: "FK_GuestAnketas_Cities_CityId",
                table: "GuestAnketas",
                column: "CityId",
                principalTable: "Cities",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_GuestAnketas_Drinks_DrinkId1",
                table: "GuestAnketas",
                column: "DrinkId1",
                principalTable: "Drinks",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_GuestAnketas_Cities_CityId",
                table: "GuestAnketas");

            migrationBuilder.DropForeignKey(
                name: "FK_GuestAnketas_Drinks_DrinkId1",
                table: "GuestAnketas");

            migrationBuilder.AlterColumn<int>(
                name: "DrinkId1",
                table: "GuestAnketas",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "CityId",
                table: "GuestAnketas",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AddForeignKey(
                name: "FK_GuestAnketas_Cities_CityId",
                table: "GuestAnketas",
                column: "CityId",
                principalTable: "Cities",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_GuestAnketas_Drinks_DrinkId1",
                table: "GuestAnketas",
                column: "DrinkId1",
                principalTable: "Drinks",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
