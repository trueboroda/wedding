using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WeddingInvitation.Models.Auth;

namespace WeddingInvitation.Abstractions
{
    public interface IAuthenticator
    {
        AuthenticationResult Authenticate(string username, string password);
    }
}
