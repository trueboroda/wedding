using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WeddingInvitation.Abstractions
{
    public interface IHashService
    {
        string HashPassword(string password);

        string HashPassword(string password, string saltValue);

        bool VerifyPassword(string validHashedPassword, string password);
    }
}
