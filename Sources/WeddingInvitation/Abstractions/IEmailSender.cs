using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WeddingInvitation.Abstractions
{
    public interface IEmailSender
    {
        /// <summary>
        /// Отправляет сообщение на указанный адрес
        /// </summary>
        /// <param name="email">Адрес получателя</param>
        /// <param name="message">Сообщение</param>
        bool Send(string email, string subject, string message);
    }
}
