import { Component } from '@angular/core';
import { BsLocaleService } from 'ngx-bootstrap';

@Component({
  selector: 'wi-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
    constructor(private localService: BsLocaleService) {
        this.localService.use("ru");
    }
}
