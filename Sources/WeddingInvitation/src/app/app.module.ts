import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { CustomFormsModule } from 'ng2-validation';
//import {  BootstrapSwitchModule } from 'angular2-bootstrap-switch2';
//import { AngularFontAwesomeModule } from 'angular-font-awesome';
import { AgmCoreModule } from '@agm/core';
//import { YaCoreModuleForRoot } from "angular2-yandex-maps";
import { ClipboardModule } from 'node_modules/ngx-clipboard'



//ngx
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { TooltipModule } from 'ngx-bootstrap/tooltip';
import { ModalModule } from 'ngx-bootstrap/modal';
import { ButtonsModule } from 'ngx-bootstrap/buttons';
import { PaginationModule } from 'ngx-bootstrap/pagination';
import { AlertModule } from 'ngx-bootstrap/alert';
import { AccordionModule } from 'ngx-bootstrap/accordion';
import { BsDatepickerModule, BsLocaleService } from 'ngx-bootstrap/datepicker';
import { ruLocale, } from 'ngx-bootstrap/locale';
import { defineLocale } from "ngx-bootstrap";
defineLocale("ru", ruLocale);



//common-comps
import { AlertsComponent } from './comps/common/alerts/alerts-component';
import { ConfirmModalComponent } from './comps/common/confirm-modal/confirm-modal.component';
import { TextFilterComponent } from './comps/common/text-filter/text-filter.component';
import { ThWithSortComponent } from './comps/common/th-with-sort/th-with-sort.component';
import { SortSelectComponent } from 'src/app/comps/common/sort-select/sort-select.component';

//comps
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AdminComponent } from './comps/admin/admin.component';
import { NotFoundComponent } from './comps/not-found/not-found.component';
import { InviteComponent } from './comps/invite/invite.component';
import { MenuComponent } from './comps/admin/menu/menu.component';
import { LoginComponent } from './comps/admin/login/login.component';
import { GuestsComponent } from './comps/guests/guests.component';
import { GuestEditComponent } from './comps/guests/edit/guest-edit.component';
import { InviteMenuComponent } from './comps/invite/menu/invite-menu.component';
import { PhotobookComponent } from './comps/photobook/photobook.component';
import { InviteAnketaComponent } from './comps/invite/anketa/invite-anketa.component';
import { InviteTimingComponent } from './comps/invite/timing/invite-timing.component';
import { GuestsReviewComponent } from 'src/app/comps/admin/guests-review/guests-review.component';
import { ReportsComponent } from 'src/app/comps/admin/reports/reports.component';
import { AnswerReportComponent } from 'src/app/comps/admin/answer-report/answer-report.component';
import { DrinksReportComponent } from 'src/app/comps/admin/drinks-report/drinks-report.component';
import { CityReportComponent } from 'src/app/comps/admin/city-report/city-report.component';
import { GuestsFilterComponent } from 'src/app/comps/guests-filter/guests-filter.component';
import { ThreeStateCheckboxComponent } from 'src/app/comps/common/three-state-checkbox/three-state-checkbox.component';

@
NgModule({
    declarations: [
        AlertsComponent, ConfirmModalComponent, TextFilterComponent, ThWithSortComponent, SortSelectComponent, ThreeStateCheckboxComponent,
        AppComponent,
        AdminComponent,
        NotFoundComponent,
        InviteComponent,
        MenuComponent,
        LoginComponent,
        GuestsComponent,
        GuestEditComponent,
        InviteMenuComponent,
        PhotobookComponent,
        InviteAnketaComponent,
        InviteTimingComponent,
        ReportsComponent,
        GuestsReviewComponent,
        AnswerReportComponent,
        DrinksReportComponent,
        CityReportComponent,
        GuestsFilterComponent

    ],
    imports: [
        BrowserModule,
        AppRoutingModule,
        FormsModule,
        HttpModule,
        CustomFormsModule,
        //AngularFontAwesomeModule,
        //toogle switch       
        // BootstrapSwitchModule.forRoot(),
        BrowserAnimationsModule,
        AgmCoreModule.forRoot({
            apiKey: "AIzaSyBirLEsn4bFxuDtm8Lw1q-MAEHPSSw7W0M"
        }),
        //YaCoreModuleForRoot(),

        ClipboardModule,

        //ngx-bootsrap
        BsDropdownModule.forRoot(),
        TooltipModule.forRoot(),
        ModalModule.forRoot(),
        ButtonsModule.forRoot(),
        PaginationModule.forRoot(),
        AlertModule.forRoot(),
        AccordionModule.forRoot(),
        BsDatepickerModule.forRoot()
        
    ],
    providers: [BsLocaleService],
    bootstrap: [AppComponent]
})
export class AppModule { }
