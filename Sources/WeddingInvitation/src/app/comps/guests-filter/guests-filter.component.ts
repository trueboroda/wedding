import { Component, OnInit, Input, Output, EventEmitter, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { GuestFilter } from 'src/app/models/guest';

@Component({
    selector: 'wi-guests-filter',
    templateUrl: './guests-filter.component.html',
    styleUrls: ['./guests-filter.component.css']
})
export class GuestsFilterComponent implements OnInit {


    @ViewChild("searchForm")
    private searchForm: NgForm;

    @Input() searchFilter: GuestFilter;

    constructor() { }

    ngOnInit() {

    }


    @Output() filterChanged = new EventEmitter<GuestFilter>();

    onFilterChanged() {
        this.filterChanged.emit(this.searchFilter);
    }

    @Output() filterCleared = new EventEmitter();
    onFilterCleared() {
        this.searchForm.reset();
        this.filterCleared.emit();
    }

}
