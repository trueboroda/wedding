import { Component, OnInit } from '@angular/core';
import { Guest } from 'src/app/models/guest';
import { BaseListComponent } from '../BaseListComponent';
import { HttpService } from 'src/app/services/http.service';
import { AlertsService } from 'src/app/services/alerts.service';
import { SimpleTextFilter } from 'src/app/common/core';
import { simpleClone } from '../../common/commonHelpers';
import { ClipboardService } from 'node_modules/ngx-clipboard'
import { ConfigProviderService } from '../../services/config-provider.service';


@Component({
    selector: 'wi-guests',
    templateUrl: './guests.component.html',
    styleUrls: ['./guests.component.css']
})
export class GuestsComponent extends BaseListComponent implements OnInit {

 
    tableMode = true;
    selectedGuest: Guest = null;
    guestForEdit: Guest = new Guest();
    guests: Guest[] = [];

    constructor(private httpService: HttpService, private readonly alertsService: AlertsService, private cbService: ClipboardService, private config: ConfigProviderService) {
        super();
    }

    ngOnInit() {
        this.search = new SimpleTextFilter();
        this.getItems();
    }


    protected getItems(): void {
        this.getGuests();
    }
    protected createSearchFilter() {
        return new SimpleTextFilter();
    }
    protected hasSearchParams(): boolean {
        return this.search != null && !!(<SimpleTextFilter>this.search).text;
    }


    getGuests() {
        this.prepareFilterForGetItems();
        this.httpService.getGuests(this.filter)
            .subscribe(page => {
                    this.guests = page.items;
                    this.totalCount = page.totalCount;
                },
                er => console.error(er)
            );
    }

    addGuest() {
        this.guestForEdit = new Guest();
        this.tableMode = false;
    }


    editGuest(selected: Guest) {
        this.selectedGuest = selected;
        this.guestForEdit = simpleClone(this.selectedGuest);
        this.tableMode = false;
    }

    deleteGuest(guest: Guest) {
        this.httpService.deleteGuest(guest.id).subscribe(x => {
            this.alertsService.success(`Пригласительная с кодом ${guest.code} успешно удалена.`);
            this.getGuests();

        },
            er => console.error(er));
    }


    //обработчики событий редактора
    guestAddedHandler(newGuest: Guest) {
        //this.orgs.push(newOrg);
        this.alertsService.success(`Новая пригласительная для гостей "${newGuest.title}" успешно добавлена с кодом ${newGuest.code}`);
        this.getItems();
        this.tableMode = true;
    }
    guestEditedHandler(guest: Guest) {
        this.alertsService.success(`Пригласительная с кодом ${guest.code} успешно отредактирована.`);
        Object.assign(this.selectedGuest, guest);
        this.selectedGuest = null;
        this.tableMode = true;
    }

    editCanceledHandler() {
        this.tableMode = true;
    }

    copyGuestRef(guest: Guest) {

        let guestRef = this.config.siteUrl + "/invite/" + guest.code;
        this.cbService.copyFromContent(guestRef);
    }

}

