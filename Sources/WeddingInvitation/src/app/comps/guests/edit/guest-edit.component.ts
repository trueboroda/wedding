import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Response } from '@angular/http';
import { NgForm, NgModel } from '@angular/forms';

import { HttpService } from 'src/app/services/http.service';
import { Guest } from 'src/app/models/guest';
import { simpleClone } from 'src/app/common/commonHelpers';


@Component({
    selector: 'wi-guest-edit',
    templateUrl: './guest-edit.component.html',
    styleUrls: ['./guest-edit.component.css']
})
export class GuestEditComponent implements OnInit {


    @Input() guest: Guest;
    @Output() onGuestAdded = new EventEmitter<Guest>();
    @Output() onGuestEdited = new EventEmitter<Guest>();
    @Output() editCanceled = new EventEmitter();


    constructor(private httpService: HttpService) { }

    ngOnInit() {
    }


    submit(form: NgForm) {
        //Добавление
        if (!this.guest.id) {
            this.httpService.addGuest(this.guest)
                .subscribe((response: Response) => {
                    let responseGuest = response.json() as Guest;
                    this.guest.id = <number>responseGuest.id;
                    let newGuest = simpleClone(this.guest);
                    this.onGuestAdded.emit(newGuest);
                    form.reset();
                    this.resetGuest();
                }
                    ,
                    error => console.error(error)
                );
        }
        //Обновление
        else {
            this.httpService.updateGuest(this.guest)
                .subscribe((data: Response) => {
                    this.onGuestEdited.emit(this.guest);
                    form.reset();
                    this.resetGuest();
                }
                    ,
                    error => console.error(error)
                );
        }
    }

    private resetGuest() {
        this.guest = new Guest();
    }

    cancelEdit(form: NgForm) {
        form.reset();
        this.resetGuest();
        this.editCanceled.emit();
    }
}
