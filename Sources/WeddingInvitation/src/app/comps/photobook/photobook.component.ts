import { Component, OnInit } from '@angular/core';
import { HttpService } from 'src/app/services/http.service';

@Component({
    selector: 'wi-photobook',
    templateUrl: './photobook.component.html',
    styleUrls: ['./photobook.component.css']
})
export class PhotobookComponent implements OnInit {

    paths: string[];

    constructor(private readonly httpService: HttpService) { }

    ngOnInit() {

        this.httpService.getPhotosPaths().subscribe(x => this.paths = x,
            er => console.error(er));

    }
}
