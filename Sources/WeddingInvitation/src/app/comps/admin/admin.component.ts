import { Component, OnInit } from '@angular/core';
import { Response } from '@angular/http';
import { Router } from '@angular/router';
import { AlertsService } from "src/app/services/alerts.service";
import { UserInfo, Roles } from 'src/app/models/user';
import { HttpService } from 'src/app/services/http.service';

@Component({
    selector: 'wi-admin',
    templateUrl: './admin.component.html',
    styleUrls: ['./admin.component.css']
})
export class AdminComponent implements OnInit {
    userInfo: UserInfo = null;
    constructor(private alertsService: AlertsService, private readonly httpService: HttpService, private readonly router: Router) { }

    ngOnInit() {
        //this.alertsService.success("Ура! Я алерт! Я работаю!");

        

        this.getUserInfo();
        

        //this.userInfo = {
        //    login : "Boroda",
        //    email : "v4@mail.ru",
        //    role : Roles.admin
        //}
    }



    getUserInfo() {
        this.httpService.getUserInfo()
            .subscribe((x) => {
                this.userInfo = x;
            },
                (er: Response) => {
                    if (er.status == 401) {
                        this.router.navigate(['login'], {
                            queryParams:
                                {
                                    returnPath: "/admin"
                                }
                        });
                    }
                    console.error(er);
                }
            );
    }

    logout() {
        this.httpService.logout().subscribe((x) => {
            console.log("Выход пользователя из системы.");
            this.userInfo = null;
            this.getUserInfo();
        },
            er => console.error(er)
        );
    }
}
