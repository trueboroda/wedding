import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { UserInfo, Roles } from 'src/app/models/user';

@Component({
    selector: 'wi-menu',
    templateUrl: './menu.component.html',
    styleUrls: ['./menu.component.css']
})
export class MenuComponent implements OnInit {
    @Input()
    userInfo: UserInfo;
    roles = Roles;


    @Output() logoutClicked = new EventEmitter();

    constructor() { }

    ngOnInit() {
    }

    logout() {
        this.logoutClicked.emit();
    }
}
