import { Component, OnInit } from '@angular/core';
import { HttpService } from 'src/app/services/http.service';
import { DrinksReport } from '../../../models/reports';
import { Guest } from 'src/app/models/guest';

@Component({
    selector: 'wi-drinks-report',
    templateUrl: './drinks-report.component.html',
    styleUrls: ['./drinks-report.component.css']
})
export class DrinksReportComponent implements OnInit {

    drinks: DrinksReport[];

    constructor(private httpService: HttpService) { }

    ngOnInit() {
        this.httpService.getDrinksReport().subscribe(x =>
            this.drinks = x
        );
    }


    getGuestRef(guest: Guest): string {

        let guestRef = "/invite/" + guest.code;
        return guestRef;
    }
}
