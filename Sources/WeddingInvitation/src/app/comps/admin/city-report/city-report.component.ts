import { Component, OnInit } from '@angular/core';
import { HttpService } from 'src/app/services/http.service';
import { CityReport } from '../../../models/reports';
import { Guest } from 'src/app/models/guest';

@Component({
    selector: 'wi-city-report',
    templateUrl: './city-report.component.html',
    styleUrls: ['./city-report.component.css']
})
export class CityReportComponent implements OnInit {
    cities: CityReport[];

    constructor(private httpService: HttpService) { }

    ngOnInit() {
        this.httpService.getCityReport().subscribe(x =>
            this.cities = x
        );
    }

    getGuestRef(guest: Guest): string {

        let guestRef = "/invite/" + guest.code;
        return guestRef;
    }
}
