import { Component, OnInit } from '@angular/core';
import { Guest } from 'src/app/models/guest';
import { BaseListComponent } from 'src/app/comps/BaseListComponent';
import { HttpService } from 'src/app/services/http.service';
import { AlertsService } from 'src/app/services/alerts.service';
import { GuestFilter } from 'src/app/models/guest';
import { SortField } from 'src/app/common/core';


@Component({
    selector: 'wi-guests-review',
    templateUrl: './guests-review.component.html',
    styleUrls: ['./guests-review.component.css']
})
export class GuestsReviewComponent extends BaseListComponent implements OnInit {
    guests: Guest[] = [];

    sortFields: SortField[] = [
          new SortField("Title", "Гости")
        , new SortField("Whose", "Жениха/невесты")
        , new SortField("IsNonresident", "Приезжие/местные.")
        , new SortField("Anketa.IsConfirmed", "Подтверждённые" )
        
    ];

    constructor(private httpService: HttpService, private readonly alertsService: AlertsService) {
        super();
    }

    ngOnInit() {
        this.search = new GuestFilter();
        this.getItems();
    }
    protected getItems(): void {
        this.getGuests();
    }
    protected createSearchFilter() {
        return new GuestFilter();
    }
    protected hasSearchParams(): boolean {
        if (this.search == null)
            return false;

        let gf = <GuestFilter>this.search;
        return !!gf.text || gf.isConfirmed != null || gf.isChanged != null || gf.isNonresident != null || gf.withPair != null || gf.withChildren != null || gf.behindTheWheel != null || gf.whose != null;
    }



    getGuests() {
        this.prepareFilterForGetItems();
        this.httpService.getGuestsReview(this.filter)
            .subscribe(page => {
                this.guests = page.items;
                this.totalCount = page.totalCount;
            },
                er => console.error(er)
            );
    }

    markAsViewed(guest: Guest) {
        this.httpService.markAnketaAsViewed(guest.id)
            .subscribe(() => {
                guest.anketa.isChanged = false;
            });
    }

    getGuestRef(guest: Guest): string {

        let guestRef = "/invite/" + guest.code;
        return guestRef;
    }

}
