import { Component, OnInit, OnChanges, OnDestroy } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';


import { HttpService } from 'src/app/services/http.service';
import { UserInfo } from '../../../models/user';

@Component({
    selector: 'wi-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit, OnDestroy {

    ngOnDestroy(): void {
        this.querySubscription .unsubscribe();
    }
    login: string;
    password: string;
    errorMessage: string
    retrurnPath: string = '';
    

    private readonly querySubscription : Subscription;

    constructor(private readonly httpService: HttpService, private readonly router: Router, private route: ActivatedRoute) {
        this.querySubscription = route.queryParams.subscribe(
            (queryParam: any) => {
                this.retrurnPath = queryParam['returnPath'];
                if (!this.retrurnPath)
                    this.retrurnPath = '/admin';
            });
    }

    ngOnInit() {
        
    }


    submit(form: NgForm) {
        if (form.valid) {
            this.httpService.login(this.login, this.password)
                .subscribe((x) => {
                    this.router.navigateByUrl(this.retrurnPath);
                },
                (er) => this.errorMessage = er);
        }
    }
}
