import { Component, OnInit } from '@angular/core';
import { HttpService } from 'src/app/services/http.service';
import { AnswersReport } from '../../../models/reports';
import { Guest } from 'src/app/models/guest';

@Component({
    selector: 'wi-answer-report',
    templateUrl: './answer-report.component.html',
    styleUrls: ['./answer-report.component.css']
})
export class AnswerReportComponent implements OnInit {
    constructor(private httpService: HttpService) { }

    answers: AnswersReport[] = [];

    ngOnInit() {

        this.httpService.getAnswersReport().subscribe(x => this.answers = x);

    }

    getGuestRef(guest: Guest): string {

        let guestRef = "/invite/" + guest.code;
        return guestRef;
    }

}
