import { Component, OnInit, OnDestroy, AfterViewInit } from '@angular/core';
import { ActivatedRoute, NavigationEnd, Router } from '@angular/router';

import {
    trigger,
    state,
    style,
    animate,
    transition,
    animation
} from '@angular/animations';

import { Subscription } from 'rxjs';
import { HttpService } from 'src/app/services/http.service';
import { Guest } from 'src/app/models/guest';

@
Component({
    selector: 'wi-invite',
    templateUrl: './invite.component.html',
    styleUrls: ['./invite.component.css'],
    animations: [
        trigger('inviteAnimation', [
            state('in', style({opacity: 1})),
            state('off', style({ opacity: 0 })),
            transition('off=>in', animate("2000ms ease-in"))
        ],           
        )
    ]
})
export class InviteComponent implements OnInit, OnDestroy
//, AfterViewInit
{
    iviteAnimationState: string = "off";

    guestCode: string;
    guest: Guest;
    private readonly paramsSub: Subscription;
    private readonly fragmentSub: Subscription;
    private fragment: string;

    constructor(private route: ActivatedRoute, private readonly httpService: HttpService, private router: Router) {
        this.paramsSub = route.params.subscribe(params => this.guestCode = params["guest_code"]);
        //     this.fragmentSub = route.fragment.subscribe(fragment => { this.fragment = fragment; });
        this.fragmentSub = router.events.subscribe(s => {
            if (s instanceof NavigationEnd) {
                const tree = router.parseUrl(router.url);
                if (tree.fragment) {
                    const element = document.querySelector("#" + tree.fragment);
                    if (element) { element.scrollIntoView(true); }
                }
            }
        });
    }

    ngOnInit() {
        this.httpService.getGuestByCode(this.guestCode).subscribe(x => {
            this.guest = x;
            this.iviteAnimationState = "in";
        },
            er => console.error(er)
        );
    }

    ngOnDestroy(): void {
        this.paramsSub.unsubscribe();
        this.fragmentSub.unsubscribe();
    }

    //ngAfterViewInit(): void {
    //    try {
    //        document.querySelector('#' + this.fragment).scrollIntoView();
    //    } catch (e) { }
    //}
}
