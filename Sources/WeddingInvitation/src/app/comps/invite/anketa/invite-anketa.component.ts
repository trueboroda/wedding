import { Component, OnInit, Input } from '@angular/core';
import { Anketa, AnketaEditModel } from 'src/app/models/Anketa';
import { NgForm } from '@angular/forms';
import { HttpService } from 'src/app/services/http.service';
import { SimpleDict } from 'src/app/models/SimpleDict';
import { Guest } from 'src/app/models/guest';
import { Router } from '@angular/router';
import { AlertsService } from 'src/app/services/alerts.service';


@Component({
    selector: 'wi-invite-anketa',
    templateUrl: './invite-anketa.component.html',
    styleUrls: ['./invite-anketa.component.css'],    
})
export class InviteAnketaComponent implements OnInit {
    

    @Input()
    guest: Guest;

    datepickerConfig = {
        dateInputFormat: "DD.MM.YYYY",
        containerClass: "theme-blue"
    }

    anketa: Anketa;
    drinks: SimpleDict[]=[];
    vineTypes: SimpleDict[];
    cities: SimpleDict[];

    alertVisibility: boolean = false;

    constructor(private readonly httpService: HttpService, private readonly alertsService: AlertsService, private router: Router) { }

    ngOnInit() {
        this.anketa = new Anketa(this.guest.id);

        this.httpService.getAnketa(this.guest.id)
            .subscribe(x => {
                if (!!x) {
                    this.anketa = x;
                }
            },
            (er) => console.error(er)
        );


        

        this.httpService.getDrinks().subscribe(x => this.drinks = x);
        this.httpService.getVineTypes().subscribe(x => this.vineTypes = x);
        this.httpService.getCities().subscribe(x => this.cities = x);

    }

    submit(form: NgForm) {
        if (form.invalid) {
            return;
        }

        let dto = new AnketaEditModel(this.anketa);
        this.httpService.mergeAnketa(dto).subscribe(
            () => this.alertVisibility = true,
            (er) => console.error(er)
            );


    }


    hideAlert() {
        this.alertVisibility = false;
    }


    confirm() {
        this.anketa.isConfirmed = true;
        this.goToFooter();       
    }

    unconfirm() {
        
        this.anketa.isConfirmed = false;
        this.goToFooter();                      
    }

    goToFooter() {
        setTimeout(() => {
            //this.router.navigate(["invite", this.guest.code], { fragment: 'footer' });
            const element = document.querySelector("#footer");
            if (element) { element.scrollIntoView(true); }
        }
        , 500 );
        
    }

}
