import { getPageSizes, Pagination, Sorting, SortDirection, FilterRequest } from "src/app/common/core";
import { simpleClone, simpleDiff } from "src/app/common/commonHelpers";



export abstract class BaseListComponent {

  protected abstract getItems ():void;
  protected abstract createSearchFilter(): any;

  //проверяет есть ли параметры поиска - ПОДЛЕЖИТ перегрузке
  protected abstract hasSearchParams(): boolean ;


  constructor() {    
  }

  

  filter = new FilterRequest();


  search: any;
  totalCount: number;
  pageSizeList = getPageSizes();
  pagination = new Pagination(this.pageSizeList[0].value, 1);
  sort = new Sorting();

  sortChangedHandler(newSort: Sorting) {
    //Значение не надо менять. т.к. менялся реальный объект по ссылке

    this.getItems();    
  }

  

  pageSizeChanged() {
    this.resetPagination();
    this.getItems();
  }


  pageChanged(eventArgs: any) {
    this.pagination.currentPage = eventArgs.page;
    this.getItems();
  }

  //сбрасывает пагинацию. переход на 1 стр
  resetPagination() {
    this.pagination.currentPage = 1;
  }

  filterApply(filter: any) {
    this.search = filter;
    this.getItems();
    //console.log(this.search);
  }

  clearFilter() {
    this.search = this.createSearchFilter();
    this.resetPagination();
    this.getItems();

    //console.log(this.search);
  }

  //проверка необходимости переустановки фильтров и сброса страницы
   needResearch() {
    if (this.filter.search === null)
      return true;
    return simpleDiff(this.search, this.filter.search);
  }



   //Подготовка фильтра к получению списка
   prepareFilterForGetItems() {
     if (this.hasSearchParams()) {
       if (this.needResearch()) {
         this.filter.search = simpleClone(this.search);
         this.resetPagination();
       }
     }
     else {
       this.filter.search = null;
     }

     if (this.sort.sortBy !== '') {
       this.filter.sort = simpleClone(this.sort);
     } else {
      this.filter.sort = null;
     }

     //пагинация
     this.filter.pagination = simpleClone(this.pagination);

   }
  

}
