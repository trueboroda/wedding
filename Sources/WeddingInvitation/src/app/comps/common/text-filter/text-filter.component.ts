import { Component, OnInit, ViewChild, Input, Output, EventEmitter } from '@angular/core';
import { NgForm } from '@angular/forms';
import { SimpleTextFilter } from "src/app/common/core";

@Component({
  selector: 'text-filter',
  templateUrl: './text-filter.component.html',
  styles: []
})
export class TextFilterComponent implements OnInit {

  @ViewChild("searchForm")
  private searchForm: NgForm;

  @Input() searchFilter: SimpleTextFilter;

  constructor() { }

  ngOnInit() {
  }

  @Output() filterChanged = new EventEmitter<SimpleTextFilter>();

  onFilterChanged() {
    this.filterChanged.emit(this.searchFilter);
  }

  @Output() filterCleared = new EventEmitter();
  onFilterCleared() {
    this.searchForm.reset();
    this.filterCleared.emit();
  }


}
