import { Component, OnInit, OnDestroy } from '@angular/core';
import { Alert, AlertType } from 'src/app/common/core';
import { AlertsService } from 'src/app/services/alerts.service';

@Component({
  selector: 'alerts',
  templateUrl: './alerts-component.html',
  styles: []
})
export class AlertsComponent implements OnInit, OnDestroy {

  alerts: Alert[] = [];

  dismissible = true;
  timeout = 20000;

  constructor(private alertsService: AlertsService) {

  }

  ngOnInit() {
    this.alertsService.alertsChanged.subscribe((alert) => {
      if (!alert) {
        // очистка если пусто
        this.alerts = [];
        return;
      }

      // если нет добавляем
      this.alerts.push(alert);
    });
  }
  

  //add() {
  //  //this.alerts.push(new Alert(AlertType.Success, "Test alert!"));
  //  this.alertsService.info("Test alert!");
  //}

  onClosed(dismissedAlert: Alert): void {
    this.alerts = this.alerts.filter(alert => alert !== dismissedAlert);
  }


  ngOnDestroy(): void {
    //отписка от события
    this.alertsService.alertsChanged.unsubscribe();
  }
}
