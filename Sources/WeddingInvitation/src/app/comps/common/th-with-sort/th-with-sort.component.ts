import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Sorting, SortDirection } from 'src/app/common/core';

@Component({
  selector: '[th-with-sort]',
  template: `     
              <a (click)="setSortBy()" [ngSwitch]="getArrow()">
                <ng-content></ng-content>
                <span *ngSwitchCase="0" class="glyphicon glyphicon-chevron-down"></span>
                <span *ngSwitchCase="1" class="glyphicon glyphicon-chevron-up"></span>
                <span *ngSwitchDefault class="text-muted glyphicon glyphicon-chevron-up"></span>
              </a>    
  `,
  styles: []
})
export class ThWithSortComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

  @Input() fieldName: string;

  @Input() sort: Sorting;
  @Output() sortChanged = new EventEmitter<Sorting>();

  onSortChanged() {
    this.sortChanged.emit(this.sort);
  }


  setSortBy = function () {
    if (this.sort.sortBy === this.fieldName) {
      this.sort.direction = this.sort.direction === SortDirection.asc ? SortDirection.desc : SortDirection.asc;
    }
    else {
      this.sort.sortBy = this.fieldName;
      this.sort.direction = 0;
    }
    this.onSortChanged();
  }


  //Получить направление стрелочки для отрисовки
  getArrow = function () {
    if (this.sort.sortBy === this.fieldName) {
      return this.sort.direction;
    }
    else {
      return -1;
    }
  }


}
