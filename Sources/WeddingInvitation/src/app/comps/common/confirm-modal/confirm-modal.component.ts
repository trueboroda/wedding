import { Component, OnInit, EventEmitter } from '@angular/core';
import {BsModalRef} from 'ngx-bootstrap/modal';


export interface IConfirmModal {
  title: string;
  question: string;
  yesClicked: EventEmitter<any>;
}

@Component({
  selector: 'confirm-modal',
  templateUrl: './confirm-modal.component.html',
  styles: []
})
export class ConfirmModalComponent implements OnInit, IConfirmModal {

  title: string = "";
  question: string = "Вы уверены?";
  yesClicked = new EventEmitter();  

  constructor( public bsModalRef: BsModalRef) { }

  ngOnInit() {    
  }

  yes() {
    
      this.yesClicked.emit();
      this.bsModalRef.hide();
    
  }

  no() {
    this.bsModalRef.hide();
  }

}
