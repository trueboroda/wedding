import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { SortField, Sorting, SortDirection } from 'src/app/common/core';

@Component({
    selector: 'sort-select',
    templateUrl: './sort-select.component.html',
    styleUrls: ['./sort-select.component.css']
})
export class SortSelectComponent implements OnInit {

    constructor() { }

    ngOnInit() {
    }

    @Input() sortFields: SortField[];
    @Input() sort: Sorting;
    @Output() sortChanged = new EventEmitter<Sorting>();

    onSortChanged() {
        this.sortChanged.emit(this.sort);
    }

   

    sortByChanged() {
        this.onSortChanged();
    }

    swithSortDirection() {
        this.sort.direction = this.sort.direction === SortDirection.asc ? SortDirection.desc : SortDirection.asc;
        this.onSortChanged();
    }
}
