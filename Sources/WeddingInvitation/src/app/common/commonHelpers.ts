


export function simpleClone(obj: any) {
  return Object.assign({}, obj);
}



export function simpleDiff(obj1, obj2) {

  for (let p in obj1) {
    if (obj1[p] instanceof Date) {
      var x = (obj1[p] - obj2[p]);
      if (x !== 0) {
        return true;
      }
    }
    else if (obj1[p] !== obj2[p]) {
      return true;
    }
  }
  return false;
}
