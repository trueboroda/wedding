export class Alert {
  constructor(public type: AlertType, public msg: string) {
  } 
}

export enum AlertType {
  Success = "success",
  Error =   "danger",
  Info =    "info",
  Warning = "warning"
}

export class ItemsPage<T> {
    items: T[];
    totalCount: number;
}


export class Pagination {

  constructor(public pageSize: number = 5, public currentPage: number = 1) {

  }
  

}

export class SortField {

    constructor(public columnName: string,
        public displayName: string) {
    }
}

export class Sorting {
  constructor(public sortBy: string = "", public direction: SortDirection = SortDirection.asc) {

  }
}


export enum SortDirection {
  none = -1,
  asc = 0,
  desc = 1

}


export class SimpleTextFilter {
  text: string = '';
}

export class FilterRequest{
  search: any = null;
  sort: Sorting = null;
  pagination: Pagination = null;
}


export function getPageSizes() {
  return [
    { value: 5, text: "5" },
    { value: 10, text: "10" },
    { value: 25, text: "25" },
    { value: 50, text: "50" },
    { value: 100, text: "100" },
    //{ value: -1, text: "ALL" }
  ];
}

export function DateTimeToDate(date) {
    return new Date(Date.UTC(date.getFullYear(), date.getMonth(), date.getDate()));
}

export function dateTimeToIsoLocalDateString(dateTime: Date): string {
    if (dateTime === null) {
        return null;
    }
    let date = DateTimeToDate(dateTime);
    let res = date.toISOString().replace("Z", "");
    return res;
}

