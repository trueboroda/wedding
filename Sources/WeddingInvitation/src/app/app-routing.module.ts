import { NgModule, Component } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AdminComponent } from 'src/app/comps/admin/admin.component';
import { LoginComponent } from 'src/app/comps/admin/login/login.component';
import { GuestsComponent } from 'src/app/comps/guests/guests.component';
import { NotFoundComponent } from 'src/app/comps/not-found/not-found.component';
import { InviteComponent } from 'src/app/comps/invite/invite.component';
import { ReportsComponent } from 'src/app/comps/admin/reports/reports.component';

const adminRoutes: Routes = [
    { path: 'guests', component: GuestsComponent },
    { path: 'reports', component: ReportsComponent }
];


const routes: Routes = [
    { path: "invite/:guest_code", component: InviteComponent },
    { path: "admin", component: AdminComponent },
    { path: "admin", component: AdminComponent, children: adminRoutes },
    { path: "login", component: LoginComponent },    
    { path: "**", component: NotFoundComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
