import  {Guest} from './guest'


export class AnswersReport {
    answer: string;
    anketCount: number;
    guestsCount: number;
    childrenCount: number;
    guests: Guest[];
}


export class DrinksReport {
    drink: string;
    count: number;    
    guests: Guest[];
}

export class CityReport {
    city: string;
    anketCount: number;
    guestsCount: number;
    childrenCount: number;
    guests: Guest[];
}
