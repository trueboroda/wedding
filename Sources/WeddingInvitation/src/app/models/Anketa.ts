import { dateTimeToIsoLocalDateString } from "../common/core";

export class Anketa {    
    constructor(public guestId: number) { }
    isConfirmed: boolean = null;
    creationTime: Date;
    changingTime: Date;
    isChanged: boolean;
    phones: string;
    comment: string;
    withPair: boolean = null;
    withChildren: boolean = false;
    behindTheWheel: boolean = false;
    drinkId1: number = null;
    vineTypeId1: number = null;
    drinkId2: number = null;
    vineTypeId2: number = null;
    cityId: number = null;
    arrivalDate: Date = null;
    departureDate: Date = null;    
}


export class AnketaEditModel {
    constructor(anketa: Anketa) {
        this.guestId            = anketa.guestId          ;
        this.isConfirmed        = anketa.isConfirmed      ;
        this.phones             = anketa.phones           ;
        this.comment            = anketa.comment          ;
        this.withPair           = anketa.withPair         ;
        this.withChildren       = anketa.withChildren     ;
        this.behindTheWheel     = anketa.behindTheWheel   ;
        this.drinkId1           = anketa.drinkId1         ;
        this.vineTypeId1        = anketa.vineTypeId1      ;
        this.drinkId2           = anketa.drinkId2         ;
        this.vineTypeId2        = anketa.vineTypeId2      ;
        this.cityId = anketa.cityId;
        this.arrivalDate = dateTimeToIsoLocalDateString(anketa.arrivalDate);
        this.departureDate = dateTimeToIsoLocalDateString( anketa.departureDate );
    }
    guestId: number;
    isConfirmed: boolean = null;
    phones: string;
    comment: string;
    withPair: boolean = null;
    withChildren: boolean;
    behindTheWheel: boolean;
    drinkId1: number = null;
    vineTypeId1: number = null;
    drinkId2: number = null;
    vineTypeId2: number = null;
    cityId: number = null;
    arrivalDate: string;
    departureDate: string;
}
