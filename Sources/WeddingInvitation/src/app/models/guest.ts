import { Anketa } from "./Anketa";

export class Guest {
    id: number;
    code: string;
    title: string;
    whose: WhoseGuestEnum;
    isNonresident: boolean;
    description: string;

    anketa: Anketa;
}


export class GuestFilter {
    text: string = '';
    isConfirmed: boolean = null;
    whose: WhoseGuestEnum = null;
    isNonresident: boolean = null;
    isChanged: boolean = null;
    withPair: boolean = null;
    withChildren: boolean = null;
    behindTheWheel: boolean = null;
}

export enum WhoseGuestEnum {
    Groom = 0,
    Bride = 1
}
