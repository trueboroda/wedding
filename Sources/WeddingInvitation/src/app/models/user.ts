

export enum Roles{
    admin = 0,
    observer = 1
}

export interface UserInfo {
    login: string;
    email: string;
    role: Roles;
}
