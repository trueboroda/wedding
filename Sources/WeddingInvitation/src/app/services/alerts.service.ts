import { Injectable, EventEmitter } from '@angular/core';
import { Router, NavigationStart } from '@angular/router';
import { Alert, AlertType } from 'src/app/common/core';

@Injectable({
  providedIn: 'root'
})
export class AlertsService {

    alertsChanged = new EventEmitter<Alert>();
    private keepAfterRouteChange = false;

    constructor(private router: Router) {

        router.events.subscribe(event => {
            if (event instanceof NavigationStart) {
                if (!this.keepAfterRouteChange) {
                    //очистка
                    this.clear();
                }
            }
        });

    }

    success(message: string, keepAfterRouteChange = false) {
        this.alert(AlertType.Success, message, keepAfterRouteChange);
    }

    error(message: string, keepAfterRouteChange = false) {
        this.alert(AlertType.Error, message, keepAfterRouteChange);
    }

    info(message: string, keepAfterRouteChange = false) {
        this.alert(AlertType.Info, message, keepAfterRouteChange);
    }

    warn(message: string, keepAfterRouteChange = false) {
        this.alert(AlertType.Warning, message, keepAfterRouteChange);
    }



    alert(type: AlertType, msg: string, keepAfterRouteChange = false) {
        this.keepAfterRouteChange = keepAfterRouteChange;
        this.alertsChanged.emit(new Alert(type, msg));
    }

    clear() {
        this.alertsChanged.emit();
    }

}
