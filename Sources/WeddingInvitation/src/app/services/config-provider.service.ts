import { Injectable } from '@angular/core';
import { HttpService } from './http.service';

@Injectable({
  providedIn: 'root'
})
export class ConfigProviderService {


    siteUrl: string;

    constructor(private httpService: HttpService) {

        this.httpService.getSeteUrl().subscribe(x => this.siteUrl = x);
    }
}
