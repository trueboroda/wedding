import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Observable } from 'rxjs';
import { map, retry } from 'rxjs/operators';
import { UserInfo } from '../models/user';
import { Guest } from '../models/guest';
import { SimpleDict } from '../models/simpleDict';
import { FilterRequest, ItemsPage } from 'src/app/common/core';
import { Anketa, AnketaEditModel } from '../models/Anketa';
import { AnswersReport, DrinksReport, CityReport } from "src/app/models/reports";



@Injectable({
  providedIn: 'root'
})
export class HttpService {

    constructor(private readonly http: Http) { }


    getSeteUrl(): Observable<string> {
        return this.http.get('api/site_url')
            .pipe(map(x => x.text()));
    }

    //reports
    //guests
    getGuestsReview(request: FilterRequest): Observable<ItemsPage<Guest>> {
        return this.http.post('api/guests/review/get_page', request)
            .pipe(map(x => x.json() as ItemsPage<Guest>));
    }

    getAnswersReport(): Observable<AnswersReport[]> {
        return this.http.get('api/reports/answers')
            .pipe(map(x => x.json() as AnswersReport[]));
    }

    getDrinksReport(): Observable<DrinksReport[]> {
        return this.http.get('api/reports/drinks')
            .pipe(map(x => x.json() as DrinksReport[]));
    }

    getCityReport(): Observable<CityReport[]> {
        return this.http.get('api/reports/city')
            .pipe(map(x => x.json() as CityReport[]));
    }

    //login n userinfo

    login(login: string, password: string): Observable<UserInfo> {
        return this.http.post('api/login', {
            login: login,
            password: password
        }).pipe(map(x =>x.json() as UserInfo));   
    }

    logout() {
        return this.http.get('api/logout');
    }


    getUserInfo(): Observable<UserInfo> {
        return this.http.get('api/userinfo')
            .pipe(map(x => x.json() as UserInfo));
    }


    //guests
    getGuests(request: FilterRequest): Observable<ItemsPage<Guest>> {
        return this.http.post('api/guests/get_page', request)
            .pipe(map(x => x.json() as ItemsPage<Guest>));
    }

    addGuest(guest: Guest) {
        return this.http.post('api/guests', guest);
    }

    updateGuest(guest: Guest) {
        return this.http.put('api/guests', guest);
    }

    deleteGuest(id: number) {
        return this.http.delete('api/guests?id=' + id);
    }

    getGuestByCode(code: string): Observable<Guest> {
        return this.http.get('api/guests/' + code)
            .pipe(map(x => x.json() as Guest))
    }

    //photobook
    getPhotosPaths(): Observable<string[]> {
        return this.http.get('api/photos')
            .pipe(map(x => x.json() as string[]));
    }

    //anketa
    getDrinks(): Observable<SimpleDict[]> {
        return this.http.get('api/anketa/drinks')
            .pipe(map(x => x.json() as SimpleDict[]));

    }

    getVineTypes(): Observable<SimpleDict[]> {
        return this.http.get('api/anketa/vinetypes')
            .pipe(map(x => x.json() as SimpleDict[]));

    }

    getCities(): Observable<SimpleDict[]> {
        return this.http.get('api/anketa/cities')
            .pipe(map(x => x.json() as SimpleDict[]));

    }

    getAnketa(guestId: number): Observable<Anketa> {
        return this.http.get('api/anketa/' + guestId)
            .pipe(map((x) => {
                let anketa = x.json() as Anketa;
                if (!!anketa) {
                    if (!!anketa.arrivalDate)
                        anketa.arrivalDate = new Date(anketa.arrivalDate);
                    if (!!anketa.departureDate)
                        anketa.departureDate = new Date(anketa.departureDate);
                }
                return anketa;
            }));
    }


    markAnketaAsViewed(idguest: number) {
        return this.http.put('api/anketa/as_viewed/' + idguest, null);
    }

    mergeAnketa(anketa: AnketaEditModel) {
        return this.http.post('api/anketa', anketa);
    }


}
