using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace WeddingInvitation.Domain
{
    public class Guest
    {
        [Key]
        public int Id { get; set; }
        
        public Guid Code { get; set; }

        [MaxLength(256)]
        public string Title { get; set; }
        
        public string Description { get; set; }

        public WhoseGuestEnum Whose { get; set; }

        public bool IsNonresident { get; set; }

        public GuestAnketa Anketa { get; set; }
        
    }
}
