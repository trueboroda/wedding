using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace WeddingInvitation.Domain
{
    public class GuestAnketa
    {
        [Key]
        public int  Id { get; set; }

        public int GuestId { get; set; }

        public Guest Guest { get; set; }

        public Boolean IsConfirmed { get; set; }

        public DateTime CreationTime { get; set; }

        public bool IsChanged { get; set; }

        public DateTime ChangingTime { get; set; }

        public string Phones { get; set; }

        public string Comment { get; set; }

        public bool? WithPair { get; set; }

        public bool WithChildren { get; set; }

        public bool BehindTheWheel { get; set; }

        public int? DrinkId1 { get; set; }
        [ForeignKey("DrinkId1")]
        public Drink Drink1 { get; set; }

        public int?  VineTypeId1 { get; set; }
        [ForeignKey("VineTypeId1")]
        public VineType VineType1 { get; set; }


        public int? DrinkId2 { get; set; }
        [ForeignKey("DrinkId2")]
        public Drink Drink2 { get; set; }

        public int? VineTypeId2 { get; set; }
        [ForeignKey("VineTypeId2")]
        public VineType VineType2 { get; set; }

        public int? CityId { get; set; }

        [ForeignKey("CityId")]
        public City City { get; set; }

        public DateTime? ArrivalDate { get; set; }

        public DateTime? DepartureDate { get; set; }



    }
}
