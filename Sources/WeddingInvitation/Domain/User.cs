using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace WeddingInvitation.Domain
{
    public class User
    {
        [Key]
        public int Id { get; set; }

        /// <summary>
        /// Логин
        /// </summary>
        [MaxLength(64)]
        public string Login { get; set; }



        /// <summary>
        /// e-mail
        /// </summary>
        [MaxLength(128)]
        public string Email { get; set; }


        [Required]
        public RolesEnum Role { get; set; }


        /// <summary>
        /// Хеш пароля
        /// </summary>
        [MaxLength(512)]
        public string PasswordHash { get; set; }
    }
}
