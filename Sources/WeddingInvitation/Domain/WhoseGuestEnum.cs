using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WeddingInvitation.Domain
{
    public enum WhoseGuestEnum
    {
        Groom  = 0,
        Bride = 1
    }
}
