using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;

namespace WeddingInvitation.Domain
{
    public enum RolesEnum
    {
        [Description("Администратор")]
        Admin = 0,

        [Description("Observer")]
        Observer = 1
    }
}
